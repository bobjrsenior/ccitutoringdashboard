"use strict";
require('dotenv').config();
//Require the sqlite3 module
var sqlite3 = require('sqlite3').verbose();
var fileSystem = require('fs');
var path = require('path');
var secureRandom = require('secure-random');

//Open the database we are using
var db = new sqlite3.Database(process.env.DATABASE);

var weeklyCourseList = [];
var weeklyRandomCodes = [];
var preCourseList = [];
var preRandomCodes = [];

main();

function main(){
    setWeeklyCourseList();
}

/**
 * Creates codes for every course in the database
 */
function setWeeklyCourseList(){
    //Go through every course
    weeklyCourseList = [];
    db.each("SELECT DISTINCT [Service] as class " +
            `FROM ${process.env.APPOINTMENTS} ` +
            "WHERE [Service] LIKE '____ ____ %'", function(error, row){
        //Add every non-error course to weeklyCourseList
        //and give it a corresponding code in weeklyRandomCodes
        if(!error){
            weeklyCourseList.push(row.class);
            weeklyRandomCodes.push(randomString());
            let courseName = row.class.split(' ').slice(0, 2).join(' ');
            if(!weeklyCourseList.includes(courseName)){
                weeklyCourseList.push(courseName);
                weeklyRandomCodes.push(randomString());
            }
        }
        else{
            console.log(error);
        }
                            
    },
    function complete(err, found) {
        //Get the preCourseList next
        setPreCourseList();
    });
}

/**
 * Creates codes for every class in the database
 */
function setPreCourseList(){
    //Go through every class
    preCourseList = [];
    db.each("SELECT DISTINCT [Class] as class " +
            `FROM ${process.env.LABELS}`, function(error, row){
        //Add every non-error class to preCourseList
        //and give it a corresponding code in preRandomCodes
        if(!error){
            preCourseList.push(row.class);
            preRandomCodes.push(randomString());
        }
        else{
            console.log(error);
        }
                            
    },
    function complete(err, found) {
        //Log the results
        logEncodedURLS();
    });
}

/**
 * Writes the full encoded urls for every course/class
 */
function logEncodedURLS(){
    //Create a string to write to code file
    let writeFile = '';
    //Add all prof (course) codes to writeFile
    writeFile += "PROF_CODES\n";
    for(let e = 0; e < weeklyCourseList.length; e = e + 1){
        let courseNoProf = weeklyCourseList[e].split(' ').slice(0, 2).join(' ');
        if(preCourseList.includes(courseNoProf)){
            writeFile += weeklyCourseList[e] + '\t' + weeklyRandomCodes[e] + '\n';
        }
    }
    
    //Add all class codes to writeFile
    writeFile += "CLASS_CODES\n";
    for(let e = 0; e < preCourseList.length; e = e + 1){
        writeFile += preCourseList[e] + '\t' + preRandomCodes[e] + '\n';
    }
    
    //Write writeFile to code file
    fileSystem.writeFile('./' + process.env.CODE_FILE, writeFile, function(error){
        if(error){
            console.log(error);
        }
    });

}

/**
 * Creates a random 10 digit base64 string
 * @return {string} - random 10 digit base64 string
 */
function randomString(){
    let finalString = "";
    //Create an array of bytes
    let byteArray = secureRandom.randomArray(10);
    //For every byte, extract a character
    byteArray.forEach(function(num){
        //Normalize the byte to just 0-63 (for each base64 char)
        let normalized = num % 64;
        //If 0-9, add digit to finalString
        if(normalized < 10){
            finalString = finalString + normalized;
        }//If 10-35, add uppercase letter to finalString
        else if(normalized < 36){
            finalString = finalString + String.fromCharCode(65 + normalized - 10);
        }//If 36-61, add lowercase letter to finalString
        else if(normalized < 62){
            finalString = finalString + String.fromCharCode(97 + normalized - 36);
        }//If 62, add _ to finalString
        else if(normalized == 62){
            finalString = finalString + '_';
        }//If 63, add - to finalString
        else{
            finalString = finalString + '-';
        }
    });
    return finalString;
}