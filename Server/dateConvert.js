"use strict";
require('dotenv').config();
//Require the sqlite3 module
var sqlite3 = require('sqlite3').verbose();

var db = new sqlite3.Database(process.env.DATABASE);

dateConvert();

function dateConvert(){
    let results = [];
    db.each(`SELECT * FROM ${process.env.APPOINTMENTS} `, function(error, row){
            if(!error){
                row['Comments'] = row['Comments'].split("'").join('"');
                row['Appointment Date'] = convertDate(row['Appointment Date']);
                results.push(row);
            }
            else{
                console.log(error);
            }
                                
        },
        function complete(err, found) {
            db.run(`DELETE FROM ${process.env.APPOINTMENTS}`);
            for(let e = 0; e < results.length; e = e + 1){
                db.run(`INSERT INTO ${process.env.APPOINTMENTS} VALUES ('${results[e]["Appointment Date"]}', '${results[e]["Appointment Time"]}', '${results[e]["Appointment Status"]}', '${results[e]["Appointment Label"]}', '${results[e]["Comments"]}', '${results[e]["Service"]}')`);
                //                                                        Appointment Date                          Appointment Time                    Appointment Status                   Appointment Label                      Comments                    Service         Cost   Resrce Name  Pmnt Name    Phone   Email    Address    City    Zip   Blank
                //console.log(`INSERT INTO TutoringAppointments VALUES ("${results[e]['Appointment Date']}", "${results[e]['Appointment Time']}", "${results[e]['Appointment Status']}", "${results[e]['Appointment Label']}", "${results[e]['Comments']}", "${results[e]['Service']}", "", "", "", "", "", "", "", "", "")`);
            }
            //db.close();
        });
}

function convertDate(oldDate){
    let newDate = "";
        
    // Check for mm/dd/yyyy
    let dateSplit = oldDate.split('/');
    if(dateSplit.length == 3){
        
        let month = dateSplit[0];
        let day = dateSplit[1];
        let year = dateSplit[2];
        if(month.length < 2){
            month = '0' + month;
        }
        if(day.length < 2){
            day = '0' + day;
        }
        newDate = year + '-' + month + '-' + day;
    }
    else{
        // Check for mm dd yyyy
        dateSplit = oldDate.split(" ");
        if(dateSplit.length == 3){
            let month = monthFromAbbr(dateSplit[0]);
            let day = dateSplit[1];
            let year = dateSplit[2];
            if(day.length < 2){
                day = '0' + day;
            }
            newDate = year + '-' + month + '-' + day;
        }// Assume it's already in the right format
        else{
            newDate = oldDate;
        }
    }
    
    return newDate;
}


function monthFromAbbr(month){
    month = month.toLowerCase();
    switch(month){
        case "jan":
            return "01";
        case "feb":
            return "02";
        case "mar":
            return "03";
        case "apr":
            return "04";
        case "may":
            return "05";
        case "jun":
            return "06";
        case "jul":
            return "07";
        case "aug":
            return "08";
        case "sep":
            return "09";
        case "oct":
            return "10";
        case "nov":
            return "11";
        case "dec":
            return "12";
        default:
            return "01";
    }
}