"use strict";
require('dotenv').config();
// Require the http module
var http = require('http');
// Require the sqlite3 module
var sqlite3 = require('sqlite3').verbose();
var fileSystem = require('fs');
var path = require('path');
var mime = require('mime');
var secureRandom = require('secure-random');
const readLine = require('readline');

// Open the database we are using
var db = new sqlite3.Database(process.env.DATABASE);

var weeklyCourseList = [];
var weeklyRandomCodes = [];
var preCourseList = [];
var preRandomCodes = [];

setWeeklyCourseList();

const PORT = process.env.PORT; 
 
function handleRequest(request, response){
    
    let requestPath = request.url;
    
    // Check for people being sneaky with urls
    if(requestPath.includes('..')){
        response.end('Dont use .. in urls...');
        console.log('INVALID URL PATH: ' + request.url);
    }
    else if(requestPath.includes('%')){
        response.end('Dont use % in urls...');
        console.log('INVALID URL PATH: ' + request.url);
    }// If it is a request for data
    else if(requestPath.includes('DATA_REQUEST')){
        // Log the request
        console.log(request.url);
        // Split the url into parts and remove the initial /
        let urlPath = request.url.split('/');
        if(urlPath.length < 2){
            response.end('Bad DATA_REQUEST ' + request.url);
        }
        urlPath = urlPath.slice(1, urlPath.length);
        // If the split is blank or doesn't have DATA_REQUEST as first param
        // It is invalid
        if(urlPath.length < 2 || urlPath[0] != 'DATA_REQUEST'){
            response.end('Bad DATA_REQUEST ' + request.url);
        }// Valid request
        else{
            // Get the request type and call the appropriate method
            let requestType = urlPath[1];
            switch(requestType){
                case "WEEKLY":
                    weeklyDataRequest(request, response, urlPath);
                break;
                case "WEEKLYLABELS":
                    weeklyLabelCountRequest(request, response, urlPath);
                break;
                case "WEEKLYCOURSELIST":
                    weeklyCourseListRequest(request, response);
                break;
                case "PRECOURSELIST":
                    preCourseListRequest(request, response);
                break;
                case "COURSENAME":
                    courseNameRequest(request, response, urlPath);
                break;
                default:
                    response.end('Invalid DATA_REQUEST Data Type ' + request.url);
                break;
            }
            
        }
        
    }// If it is not a data request (standard get request)
    else{
        // Split the url
        let pathSplit = requestPath.split('/');
        let filePath;
        // Make sure the path has enough sections to avoid an error
        if(pathSplit.length < 2){
            response.end('Bad DATA_REQUEST ' + request.url);
        }
        if(!pathSplit[1].includes('.') && pathSplit[1].length == 10){
            filePath = path.join('../Site/index.html');
        }
        else{
            filePath = path.join('../Site', requestPath);
        }
        
        // If the request is for the root dir, redirect to index.html
        if(request.url == "/"){
            filePath = path.join(filePath, 'index.html');
        }
        // Log the file path
        console.log(filePath);
        // Check if there is read access to the file
        fileSystem.access(filePath, fileSystem.R_OK, function (error){
            // If there isn't read access
            if(error){
                response.end('404 File Not Found');
                console.log('File Not Found: ' + filePath);
            }// If there is read access
            else{
                // Get the filename and MIME type
                let fileName = path.basename(filePath);
                let mimetype = mime.lookup(filePath);
                
                // Set the content type header
                response.setHeader('Content-type', mimetype);
                // Create a read stream for the requested file
                let fileStream = fileSystem.createReadStream(filePath);
                // When done with the file, close the connection
                fileStream.on('close', function(){
                   response.end(); 
                });
                // Pipe the file to the response
                fileStream.pipe(response);
            }
        });
    }
    return;
}

/**
 * Fetches the number of appointments by day for a given time period
 * @param {object} request - The request from a client
 * @param {object} reponse - The response used to write to the client
 * @param {string[]} urlPath - An array of the different sections of the request path
 */
function weeklyDataRequest(request, response, urlPath){
    let startDate = '1999-01-01';
    let endDate = '2030-12-30';
    
    // If the url path doesn't have DATA_REQUEST, WEEKLY, and a course code, it's invalid
    if(urlPath.length < 3){
        response.end('Invalid DATA_REQUEST ' + request.url);
        console.log('No course name');
        return;
    }
    
    // Extra params are for start and end date
    if(urlPath.length > 3){
        // Get the start date
        let startMilliDate = timestamp(urlPath[3]);
        
        // If it's valid, set the new start date
        if(!isNaN(startMilliDate)){
            startDate = dateFormat(startMilliDate);
        }
        
        // If there is an end date
        if(urlPath.length > 4){
            // Get the end date
            let endMilliDate = timestamp(urlPath[4]);
            // If it's valid, use it
            if(!isNaN(endMilliDate)){
                endDate = dateFormat(endMilliDate);
            }
        } 
    }
    
    // Get the course code
    let courseCode = urlPath[2];
    
    // If it's invalid, end the request
    if(!weeklyRandomCodes.includes(courseCode)){
        response.end('Invalid DATA_REQUEST ' + request.url);
        console.log('Bad course code: ' + courseCode);
    }// If it's valid
    else{
        // Get the course index and request the data
        let index = weeklyRandomCodes.indexOf(courseCode);
        let results = [];

        let courseName = weeklyCourseList[index];
        if(courseName.split(' ').length == 2){
            courseName = courseName + '%';
        }
        
        db.each("SELECT [Appointment Date] as date, COUNT() as count " +
                `FROM ${process.env.APPOINTMENTS} ` +
                `WHERE [Appointment Label]  = 'Done' AND [Service] LIKE '${courseName}' ` +
                `AND [Appointment Date] >= '${startDate}' AND [Appointment Date] <= '${endDate}' `+
                "GROUP BY date", function(error, row){
            // Push all non-error data into results array
            if(!error){
                results.push(row);
            }
            else{
                console.log(error);
            }
                                
        },
        function complete(err, found) {
            // Send the results to the client
            response.end(JSON.stringify(results));
        });
        
    }
}

/**
 * Fetches course name when given a valid prof and class code
 * @param {object} request - The request from a client
 * @param {object} reponse - The response used to write to the client
 * @param {string[]} urlPath - An array of the different sections of the request path
 */
function courseNameRequest(request, response, urlPath){
    // A valid request has at least 4 parameters
    if(urlPath.length < 4){
        response.end(JSON.stringify('undefined'));
    }
    else{
        // If the codes are calid, return the course name
        if(weeklyRandomCodes.includes(urlPath[2]) && preRandomCodes.includes(urlPath[3])){
            response.end(JSON.stringify(weeklyCourseList[weeklyRandomCodes.indexOf(urlPath[2])]));
        }// If invalid, return undefined
        else{
            response.end(JSON.stringify('undefined'));
        }
    }
}

/**
 * Fetches prof courses in the database
 * @param {object} request - The request from a client
 * @param {object} reponse - The response used to write to the client
 */
function weeklyCourseListRequest(request, response){
    // Get all the courses into a string
    let courseList = [];
    for(let e = 0; e < weeklyCourseList.length; e = e + 1){
        courseList.push(weeklyCourseList[e].split(' ').join('_'));
    }
    // Return courses to client
    response.end(JSON.stringify(courseList));
}

/**
 * Fetches classes in the database
 * @param {object} request - The request from a client
 * @param {object} reponse - The response used to write to the client
 */
function preCourseListRequest(request, response){
    // Get all the classes into a string
    let courseList = [];
    for(let e = 0; e < preCourseList.length; e = e + 1){
        courseList.push(preCourseList[e].split(' ').join('_'));
    }
    // Return classes to client
    response.end(JSON.stringify(courseList));
}

/**
 * Fetches the number of times a label was used for a specific course over a period of time
 * @param {object} request - The request from a client
 * @param {object} reponse - The response used to write to the client
 * @param {string[]} urlPath - An array of the different sections of the request path
 */
function weeklyLabelCountRequest(request, response, urlPath){
    let startDate = '1999-01-01';
    let endDate = '2030-12-30';
    // A valid request has at least 4 parameters
    if(urlPath.length < 4){
        response.end('Invalid DATA_REQUEST ' + request.url);
        console.log('No course name');
        return;
    }
    // Extra parameters are startDate and endDate
    if(urlPath.length > 4){
        // First extra parameter is startDate
        let startMilliDate = timestamp(urlPath[4]);
        // If it's valid, use it, else use default dtartDate
        if(!isNaN(startMilliDate)){
            startDate = dateFormat(startMilliDate);
        }
        else{
            startMilliDate = timestamp(startDate);
        }
        // If there is an endDate
        if(urlPath.length > 5){
            // If the endDate is valid, use it
            let endMilliDate = timestamp(urlPath[5]);
            if(!isNaN(endMilliDate)){
                endDate = dateFormat(endMilliDate);
            }
        } 
    }
    // Get the course and class codes
    let courseCode = urlPath[2];
    let courseNoProfCode = urlPath[3];
    // If either are invalid, end the connection
    if(!preRandomCodes.includes(courseNoProfCode) || !weeklyRandomCodes.includes(courseCode)){
        response.end('Invalid DATA_REQUEST' + request.url);
        console.log('Bad course name: ' + courseCode);
    }// If codes are valid
    else{
        // Get there index
        let preIndex = preRandomCodes.indexOf(courseNoProfCode);
        let weeklyIndex = weeklyRandomCodes.indexOf(courseCode);
        // Collect labels for the course/class
        let labels = [];
        db.each("SELECT [Label] as label " +
                `FROM ${process.env.LABELS} ` +
                `WHERE [Class] = '${preCourseList[preIndex]}'`, function(error, row){
            // Push non-error rows into labels
            if(!error){
                labels.push(row.label);
            }
            else{
                console.log(error);
            }
                                
        },
        function complete(err, found) {
            let results = [];
            let finished = 0;
            let courseName = weeklyCourseList[weeklyIndex];
            if(courseName.split(' ').length == 2){
                courseName = courseName + '%';
            }
            // Retrieve every comment for the time period
            db.each(`SELECT Comments as comment ` +
            `FROM ${process.env.APPOINTMENTS} ` +
            `WHERE [Service] LIKE '${courseName}' AND [Appointment Label] = 'Done' ` +
            `AND [Appointment Date] >= '${startDate}' AND [Appointment Date] <= '${endDate}' COLLATE NOCASE`, function(error, row){
                // Push non-error rows into results
                if(!error && row['comment'].length > 1){
                    // Split the comment into parts
                    var splitArray = row['comment'].split(';');
                    var found = false;
                    // For every part, check for which label it belongs to
                    splitArray.forEach(function(value){
                        value = value.trim();
                        // Make sure to ignore empty tags
                        if(value.length > 2){
                            // Compare with every label
                            for(var e = 0; e < labels.length; e = e + 1){
                                if(value.toUpperCase() === (labels[e].toUpperCase())){
                                    found = true;
                                    // Null checking
                                    if(results[e] == null){
                                        results[e] = 0;
                                    }
                                    else{
                                        // Increment the label count
                                        results[e] = results[e] + 1;
                                    }
                                }
                            }
                        }
                    });
                    // If it wasn't for any label, it's for Others
                    if(!found){
                        
                        // Null checking
                        if(results[labels.length] == null){
                            results[labels.length] = 0;
                        }
                        else{
                            // Increment the others count
                            results[labels.length] = results[labels.length] + 1;
                        }
                    }
                }
                else{
                    if(error){
                        console.log(error);
                    }
                }
                                    
            },
            function complete(err, found) {
                var finalResults = [];
                // Aggregate the final results into one array
                for(var e = 0; e < labels.length; e = e + 1){
                    // Add a label only if it was used
                    if(results[e] != null && results[e] != 0){
                        finalResults.push({label: labels[e], count: results[e]});
                    }
                }
                // If there are any others, push them
                if(results[results.length - 1] != null && results[results.length - 1] != 0){
                    finalResults.push({label: 'Others', count: results[results.length - 1]});
                }
                response.end(JSON.stringify(finalResults));
            });
            
        });
    }
}

var server = http.createServer(handleRequest);

// Lets start our server
server.listen(PORT, function(){
    console.log("Server listening on: http://localhost:%s", PORT);
});

 
 /**
 * Fetches courses available in the database
 */
function setWeeklyCourseList(){
    // Retrieve the courses
    weeklyCourseList = [];
    db.each("SELECT DISTINCT [Service] as class " +
            `FROM ${process.env.APPOINTMENTS} ` +
            "WHERE [Service] LIKE '____ ____ %'", function(error, row){
        // Push every non-error course into weeklyCOurseList
        if(!error){
            weeklyCourseList.push(row.class);
            let courseName = row.class.split(' ').slice(0, 2).join(' ');
            if(!weeklyCourseList.includes(courseName)){
                weeklyCourseList.push(courseName);
            }
        }
        else{
            console.log(error);
        }
                            
    },
    function complete(err, found) {
        // Set preCourseList after done with weeklyCourseList`
        setPreCourseList();
    });
}

/**
 * Fetches classes available in the database
 */
function setPreCourseList(){
    // Retrieve classes
    preCourseList = [];
    db.each("SELECT DISTINCT [Class] as class " +
            `FROM ${process.env.LABELS}`, function(error, row){
        // Push every non-error class into preCourseList
        if(!error){
            preCourseList.push(row.class);
        }
        else{
            console.log(error);
        }
                            
    },
    function complete(err, found) {
        // Now that everything is retrieved, grab the encodings
        retrieveEncodedURLS();
    });
}

/**
 * Retrieves the codes for courses and classes
 */
function retrieveEncodedURLS(){
    // Create a stream to read the file containing codes
    let fileStream = fileSystem.createReadStream('./' + process.env.CODE_FILE);
    let scanner = readLine.createInterface({
       input: fileStream 
    });
    let PROF_CODES = 0;
    let CLASS_CODES = 1
    // Set what type of code we are reading
    let inputType = PROF_CODES;
    
    // For each line in the file
    scanner.on('line', (line) => {
        // If PROF_CODES, then we are reading course (prof) codes
        if(line == "PROF_CODES"){
            inputType = PROF_CODES;
        }// If CLASS_CODES, then we are reading class codes
        else if(line == "CLASS_CODES"){
            inputType = CLASS_CODES;
        }
        else{
            // Split the line ([0] = name, [1] = code)
            let lineSplit = line.split('\t');
            // Write code into different code list depending on inputType
            switch(inputType){
                case PROF_CODES:
                    if(weeklyCourseList.includes(lineSplit[0])){
                        weeklyRandomCodes[weeklyCourseList.indexOf(lineSplit[0])] = lineSplit[1];
                    }
                break;
                case CLASS_CODES:
                    if(preCourseList.includes(lineSplit[0])){
                        preRandomCodes[preCourseList.indexOf(lineSplit[0])] = lineSplit[1];
                    }
                break;
            }
        }
       
    });
    
    scanner.on('close', () => {
        // Once done, log the encoded urls so that they can be sent to professors
        logEncodedURLS();
    });
}

/**
 * Outputs all of the encoded urls to the console
 */
function logEncodedURLS(){
    console.log('Course Encoded URLS:');
    // Hold all course urls
    let urls = []
    // For every course
    for(let e = 0; e < weeklyCourseList.length; e = e + 1){
        // Grab it's corresponded class
        let courseNoProf = weeklyCourseList[e].split(' ').slice(0, 2).join(' ');
        
        // If the class also exists, add the url to the urls array
        if(preCourseList.includes(courseNoProf)){
            urls.push(weeklyCourseList[e] + ' : ' + weeklyRandomCodes[e] + '/' + preRandomCodes[preCourseList.indexOf(courseNoProf)]);
        }
    }
    
    // Sort course urls
    urls.sort((a, b) => {
        return a.localeCompare(b);
    });
    
    // Print course urls
    for(let e = 0; e < urls.length; e = e + 1){
        console.log(urls[e]);
    }
}

/**
 * Converts a millisecond timestamp into its corresponding string ISO 8601 format (yyyy-mm-dd)
 * @param {long} dateMilli - millisecond timestamp
 * @return {string} - 
 */
function dateFormat(dateMilli){
    // Create a date from the timestamp and extract year, month, day
    let date = new Date(dateMilli);
    let month = '' + (date.getMonth() + 1);
    let day = '' + (date.getDate());
    let year = date.getFullYear();
    
    // Pad 0s if necessary
    if(month.length < 2){
        month = '0' + month;
    }
    if(day.length < 2){
        day = '0' + day;
    }
    // Return the reformatted date
    return year + '-' + month + '-' + day;
}

/**
 * Converts an ISO 8601 format (yyyy-mm-dd) date into its corresponding millisecond timestamp
 * @param {string} date - ISO 8601 format (yyyy-mm-dd) date
 * @return {long} - millisecond timestamp for the corresponding date
 */
function timestamp(date){
    // Spit the date string into parts
    let dateSplit = date.split('-');
    // Create the timestamp based on the string contents
    if(dateSplit.length == 3){
        return new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]).getTime();
    }
    else if(dateSplit.length == 2){
        return new Date(dateSplit[0], dateSplit[1] - 1).getTime();
    }
    else if(dateSplit.length == 1){
        return new Date(dateSplit[0]).getTime();
    }
    return NaN;
}
