"use strict";
var activeCourse = "undefined";
var weeklyCode;
var preCode;
var courseDisplay;
var courseDropdown;
var dateSlider;
var weeklyChart;
var frequencyChart;
var noFrequencyData;
var noWeeklyData;
var activeSemesterIndex = 0;
var semesterDates = [['Fall 2016', '2016-08-22', '2016-12-19'],['Spring 2017', '2017-01-01', '2017-05-15']];
var startDate = semesterDates[activeSemesterIndex][1];
var endDate = semesterDates[activeSemesterIndex][2];
var minDate = startDate;
var maxDate = endDate;
var weeklyData = [[],[[]]];
var frequencyData = [];
var frequencyMax = 0;
var weeklyLoading = false;
var frequencyLoading = false;

window.onload = function() {
    courseDisplay = document.getElementById('classDisplay');
    courseDropdown = document.getElementById('classDropdown');
    noFrequencyData = document.getElementById('noFrequencyData');
    noWeeklyData = document.getElementById('noWeeklyData');

    generateWeeklyChart();

    generateDateSlider(startDate, endDate);
    generateFrequencyChart();
    var url = window.location.href;
    var urlSplit = url.split('/');
    weeklyCode = urlSplit[3];
    preCode = urlSplit[4];
    
    getCourseName(setActiveCourse);
    
};

function initCharts(course) {
    getWeeklyData(updateWeeklyChart);
    
    getWeeklyFrequencies(updateFrequencyChart);

}

function generateWeeklyChart(){

        weeklyChart = c3.generate({
        bindto: '#chart',
        data: {
            columns: weeklyData[1],
            type: 'bar',
            color: function (color, d) {

                if(typeof d.index == 'undefined'){
                    return '#31a762';
                }
                else if(d.index < weeklyData[0].length && weeklyData[0][d.index].localeCompare(startDate) >= 0 && weeklyData[0][d.index].localeCompare(endDate) <= 0){
                    return '#31a762';
                }
                else{
                    return '#808080';
                }
                
            }
        },
        collumns: {
            width: {
                ratio: 0.5
            }
        },
        axis: {
            x: {
                tick: {
                  format: function (x) {
                      var dateSplit = weeklyData[0][x].split('-');
                      return dateSplit[1] + '/' + dateSplit[2] + '/' + dateSplit[0];
                  }
                },
                label: {
                   text: 'Appointment Date',
                   position: 'outer-center',
                }
            },
            y: {
                label: {
                   text: 'Number of Appointments',
                   position: 'outer-middle',
                }
            }
        },
        tooltip: {
            grouped: true
        },
        title: {
            text: 'Frequency of Appointments By Date'
        }
    });
}

function updateWeeklyChart(rawData){
    rawData.sort(function (a, b) {
        var d1 = timestamp(a.date);
        var d2 = timestamp(b.date);
        return d1 - d2;
    });
    var millisInADay = 1000 * 60 * 60 * 24;
    
    var data = [[],[[]]];
    
    data[1][0].push(activeCourse.split(' ').slice(0, 2).join(' '));
    if(rawData.length > 0 && rawData[0].date != semesterDates[activeSemesterIndex][1]){
        var firstStamp = timestamp(rawData[0].date);
        var startStamp = timestamp(semesterDates[activeSemesterIndex][1]);
        for(; startStamp < firstStamp; startStamp = startStamp + millisInADay){
            data[0].push(standardDateFormat(startStamp));
            data[1][0].push(0);
        }        
    }
    if(rawData.length > 0){
        data[0].push(rawData[0].date);
        data[1][0].push(rawData[0].count);
    }
    for(var e = 1; e < rawData.length; e = e + 1){
        var curStamp = timestamp(rawData[e - 1].date);
        var endStamp = timestamp(rawData[e].date);
        var difference = endStamp - curStamp;
        if(difference > millisInADay){
            var days = Math.round(difference / millisInADay);
            curStamp = curStamp + millisInADay;
            for(; curStamp < endStamp; curStamp = curStamp + millisInADay){
                data[0].push(standardDateFormat(curStamp));
                data[1][0].push(0);
                
            }
        }
        data[0].push(rawData[e].date);
        data[1][0].push(rawData[e].count);
    }
    
    if(rawData.length > 0 && rawData[0].date != semesterDates[activeSemesterIndex][2]){
        var lastStamp = timestamp(rawData[rawData.length - 1].date) + millisInADay;
        var endStamp = timestamp(semesterDates[activeSemesterIndex][2]);
        for(; lastStamp < endStamp; lastStamp = lastStamp + millisInADay){
            data[0].push(standardDateFormat(lastStamp));
            data[1][0].push(0);
        }        
    }
    
    if(rawData.length > 0){
        noWeeklyData.style.visibility = 'hidden';
    }
    else{
        console.log('NO DATA');
        noWeeklyData.style.visibility = 'visible';
    }
    reloadNewWeeklyChartdata(data);
        
    updateDateSlider(startDate, endDate);
} 

function reloadWeeklyChart(){
    if(!weeklyLoading){
        weeklyLoading = true;
        weeklyChart.unload({done: function() {
              weeklyChart.load({columns: weeklyData[1], done: function(){
                    weeklyLoading = false;
                }
                });
        }});
    }
    else{
        setTimeout(function(){
            reloadWeeklyChart();
        }, 100);
    
    }
}

function reloadNewWeeklyChartdata(data){
        if(!weeklyLoading){
        weeklyLoading = true;
        weeklyChart.unload({done: function() {
            weeklyData = data;
            weeklyChart.load({columns: weeklyData[1], done: function(){
                weeklyLoading = false;
            }
            });
        }});
    }
    else{
        setTimeout(function(){
            reloadNewWeeklyChartdata(data);
        }, 100);
    
    }
}

function generateFrequencyChart(){
    var colorPallete = ['#000000', '#0000AA', '#00AA00', '#00AAAA', '#AA0000', '#AA00AA', '#AA5500', '#AAAAAA', '#555555', '#5555FF', '#55FF55', '#55FFFF', '#FF5555', '#FF55FF', '#AAAA00'];
    frequencyChart = c3.generate({
        bindto: '#tagFrequencies',
        data: {
            columns: frequencyData,
            type: 'pie',
            color: function (color, d) {
                var keyword = '';
                if(d.id){
                    keyword = d.id;
                }
                else{
                    keyword = d;
                }
                for(var index = 0; index < frequencyData.length; index = index + 1){
                    if(frequencyData[index][0] == d){
                        return colorPallete[index % colorPallete.length];
                    }
                }
                return '#ff0000';   
            }
        },
        collumns: {
            width: {
                ratio: 0.5
            }
        },
        axis: {
          x: {
            tick: {
                format: function (x) { return 'Topics'; }
            }
          },
          y: {
              max: frequencyMax
          }
        },
        tooltip: {
            grouped: false
        },
        title: {
            text: 'Topics Covered'
        },
        legend: {
            position: 'right'
        }
    });

}

function updateFrequencyChart(rawData){
    
    rawData.sort(function (a, b) {
        return a.label.localeCompare(b.label);
    });
    
    var foundData = false;
    
    var data = [];
    for(var e = 0; e < rawData.length; e = e + 1){
        data.push([rawData[e].label, rawData[e].count]);
        if(rawData[e].count > 0){
            foundData = true;
        }
    }
    
    if(foundData){
        noFrequencyData.style.visibility = 'hidden';
    }
    else{
        noFrequencyData.style.visibility = 'visible';
    }
    
    reloadNewFrequencyData(data);
     
}

function reloadFrequencyData(){
    if(!frequencyLoading){
        frequencyLoading = true;
        frequencyChart.unload({done: function() {
            
            frequencyChart.load({columns: frequencyData, done: function(){
                frequencyLoading = false;
            }
            });
        }});
    }
    else{
        setTimeout(function(){
            reloadFrequencyData();
        }, 100);
    
    }
}

function reloadNewFrequencyData(data){
    if(!frequencyLoading){
        frequencyLoading = true;
        frequencyChart.unload({done: function() {
            frequencyData = data;
            frequencyChart.load({columns: frequencyData, done: function(){
                frequencyLoading = false;
            }
            });
        }});
    }
    else{
        setTimeout(function(){
            reloadNewFrequencyData(data);
        }, 100);
    
    }
}

function generateDateSlider(sliderStartDate, sliderEndDate){
    dateSlider = document.getElementById('dateSlider');
    var dateValues = [
        document.getElementById('startDate'),
        document.getElementById('endDate')
    ];
    
    var startDateSplit = sliderStartDate.split('-');
    var endDateSplit = sliderEndDate.split('-');
   
    dateValues[0].innerHTML = "Start Date: " + startDateSplit[1] + '/' + startDateSplit[2] + '/' + startDateSplit[0];
    dateValues[1].innerHTML = "End Date: " + endDateSplit[1] + '/' + endDateSplit[2] + '/' + endDateSplit[0];

    noUiSlider.create(dateSlider, {
        /* Create two timestamps to define a range. */
        range: {
            min: timestamp(sliderStartDate),
            max: timestamp(sliderEndDate)
        },
        connect: true,
        behaviour: 'tap-drag',
        /* Steps of one day */
        step: 1 * 24 * 60 * 60 * 1000,
        /* Two more timestamps indicate the handle starting positions. */
        start: [ timestamp(sliderStartDate), timestamp(sliderEndDate) ]
    });
    dateSlider.noUiSlider.on('change', function( values, handle ) {
        var dateChanged = false;
        var newDate = standardDateFormat(values[0]);
        if(startDate != newDate){
            dateValues[0].innerHTML = 'Start Date: ' + dateDisplayFormat(values[0]);
            dateChanged = true;
            startDate = newDate; 
        }            
        
        newDate = standardDateFormat(values[1]);
        if(endDate != newDate){
            dateValues[1].innerHTML = 'End Date: ' + dateDisplayFormat(values[1]);
            dateChanged = true;
            endDate = newDate;
        } 
        
        if(weeklyChart && dateChanged){
            reloadWeeklyChart();
        }
        getWeeklyFrequencies(updateFrequencyChart);

    });
}

function updateDateSlider(sliderStartDate, sliderEndDate){
    if(dateSlider){
        if(sliderStartDate == sliderEndDate){
            dateSlider.style.visibility = 'hidden';
        }
        else{
            dateSlider.style.visibility = 'visible';
            dateSlider.noUiSlider.updateOptions({
                range: {
                    'min': timestamp(sliderStartDate),
                    'max': timestamp(sliderEndDate)
                },
                start: [timestamp(sliderStartDate), timestamp(sliderEndDate)]
            });
        }
    }
    else{
        setTimeout(function(){
            updateDateSlider(sliderStartDate, sliderEndDate);
        }, 100);
    }
    
}

function setActiveCourse(course){
    courseDisplay.innerHTML = course.split(' ').slice(0, 2).join(' ') + ' ' + semesterDates[activeSemesterIndex][0];
    activeCourse = course;
    initCharts();
}

function setSemester(semesterIndex){
    activeSemesterIndex = semesterIndex;
    startDate = semesterDates[activeSemesterIndex][1];
    endDate = semesterDates[activeSemesterIndex][2];
    courseDisplay.innerHTML = activeCourse.split(' ').slice(0, 2).join(' ') + ' ' + semesterDates[activeSemesterIndex][0];
    initCharts();
}

function getCourseName(callback){
    httpRequest('../DATA_REQUEST/COURSENAME/' + weeklyCode + '/' + preCode, callback);
}

function getWeeklyData(callback) {
    httpRequest('../DATA_REQUEST/WEEKLY/' + weeklyCode + '/' + startDate + '/' + endDate, callback);
}

function getWeeklyFrequencies(callback){
    httpRequest('../DATA_REQUEST/WEEKLYLABELS/' + weeklyCode + '/' + preCode + '/' + startDate + '/' + endDate, callback);
}

function httpRequest(url, callback){
    var request = new XMLHttpRequest();
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
            
            var response = [];
            try {
                response = JSON.parse(request.responseText);
            } catch (e) {
                if(e){

                }
            }
            callback(response);
        }
    };
    request.open("GET", url, true);
    request.send();
}

function dateDisplayFormat(dateMilli){
    dateMilli = Math.round(dateMilli);
    var date = new Date(dateMilli);
    var month = '' + (date.getMonth() + 1);
    var day = '' + (date.getDate());
    var year = date.getFullYear();
    
    if(month.length < 2){
        month = '0' + month;
    }
    if(day.length < 2){
        day = '0' + day;
    }
    
    return month + '/' + day + '/' + year;
}

function standardDateFormat(dateMilli){
    dateMilli = Math.round(dateMilli);
    var date = new Date(dateMilli);
    var month = '' + (date.getMonth() + 1);
    var day = '' + (date.getDate());
    var year = date.getFullYear();
    
    if(month.length < 2){
        month = '0' + month;
    }
    if(day.length < 2){
        day = '0' + day;
    }
    
    return year + '-' + month + '-' + day;
}

function timestamp(date){
    var dateSplit = date.split('-');
    if(dateSplit.length == 3){
        return new Date(dateSplit[0], dateSplit[1] - 1, dateSplit[2]).getTime();
    }
    else if(dateSplit.length == 2){
        return new Date(dateSplit[0], dateSplit[1] - 1).getTime();
    }
    else if(dateSplit.length == 1){
        return new Date(dateSplit[0]).getTime();
    }
    return NaN;
}
